package com.navi;

import com.navi.exception.InvalidCommandException;
import com.navi.exception.NoSuchFileException;
import com.navi.exception.ServiceException;
import com.navi.factory.CommandExecutorFactory;
import com.navi.service.BranchManagementService;
import com.navi.service.FileReaderService;
import com.navi.service.VehicleBookingService;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("File path missing!");
            return;
        }
        BranchManagementService branchManagementService = new BranchManagementService();
        VehicleBookingService vehicleBookingService = new VehicleBookingService(branchManagementService);
        FileReaderService fileReaderService = new FileReaderService(args[0], new CommandExecutorFactory(branchManagementService,
                vehicleBookingService));
        try {
            fileReaderService.process();
        } catch (ServiceException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
