package com.navi.enums;

public enum VehicleType {

    CAR, BIKE, VAN;

    public static VehicleType fromString(String vehicleType)
    {
        for(VehicleType type : VehicleType.values()) {
            if(type.toString().equals(vehicleType)) {
                return type;
            }
        }
        return null;
    }
}
