package com.navi.exception;

public class InvalidCommandException extends ServiceException {


    public InvalidCommandException(String errorMessage) {
        super(errorMessage);
    }
}
