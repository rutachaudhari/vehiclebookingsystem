package com.navi.exception;

public class NoSuchFileException extends ServiceException {
    public NoSuchFileException(String errorMessage) {
        super(errorMessage);
    }

}
