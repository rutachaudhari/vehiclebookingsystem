package com.navi.exception;

public abstract class ServiceException extends Exception {

    protected final String errorMessage;

    protected ServiceException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }
}
