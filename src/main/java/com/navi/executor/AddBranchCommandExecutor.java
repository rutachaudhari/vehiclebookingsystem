package com.navi.executor;

import com.navi.enums.VehicleType;
import com.navi.model.Command;
import com.navi.service.BranchManagementService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AddBranchCommandExecutor extends CommandExecutor {

    public static final String ADD_BRANCH_COMMAND_NAME = "ADD_BRANCH";
    private final BranchManagementService branchManagementService;

    public AddBranchCommandExecutor(BranchManagementService branchManagementService) {
        this.branchManagementService = branchManagementService;
    }

    @Override
    public boolean isValid(Command command) {
        return ADD_BRANCH_COMMAND_NAME.equals(command.getCommandName()) &&
                command.getParams().size() == 2;
    }

    @Override
    public String execute(Command command) {
        List<String> params = command.getParams();
        String branchId = params.get(0);
        String vehicleTypes = params.get(1);
        List<VehicleType> vehicleTypeList = Arrays.stream(vehicleTypes.split("\\s*,\\s*"))
                .map(v -> VehicleType.valueOf(v.toUpperCase()))
                .collect(Collectors.toList());

        return String.valueOf(branchManagementService.addBranch(branchId, vehicleTypeList));
    }
}
