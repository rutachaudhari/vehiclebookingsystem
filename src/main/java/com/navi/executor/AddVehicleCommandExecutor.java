package com.navi.executor;

import com.navi.enums.VehicleType;
import com.navi.model.Command;
import com.navi.service.BranchManagementService;

import java.math.BigDecimal;
import java.util.List;

public class AddVehicleCommandExecutor extends CommandExecutor {

    public static final String ADD_VEHICLE_COMMAND_NAME = "ADD_VEHICLE";
    private final BranchManagementService branchManagementService;

    public AddVehicleCommandExecutor(BranchManagementService branchManagementService) {
        this.branchManagementService = branchManagementService;
    }

    @Override
    public boolean isValid(Command command) {
        return ADD_VEHICLE_COMMAND_NAME.equals(command.getCommandName()) &&
                command.getParams().size() == 4;
    }

    @Override
    public String execute(Command command) {
        List<String> params = command.getParams();
        String branchId = params.get(0);
        VehicleType vehicleType = VehicleType.fromString(params.get(1));
        String vehicleId = params.get(2);
        BigDecimal price = new BigDecimal(params.get(3));

        return String.valueOf(branchManagementService.addVehicleToBranch(branchId, vehicleType, vehicleId, price));
    }
}
