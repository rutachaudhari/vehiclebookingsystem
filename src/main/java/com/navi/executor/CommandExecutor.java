package com.navi.executor;

import com.navi.model.Command;

public abstract class CommandExecutor {

    /**
     * method to validate given command
     * @param command
     * @return
     */
    public abstract boolean isValid(Command command);

    /**
     * method to execute command with the params passed
     * @param command
     * @return
     */
    public abstract String execute(Command command);

}
