package com.navi.executor;

import com.navi.model.Command;
import com.navi.service.BranchManagementService;
import com.navi.service.VehicleBookingService;

import java.util.List;

public class DisplayVehicleCommandExecutor extends CommandExecutor {

    public static final String DISPLAY_VEHICLES_COMMAND_NAME = "DISPLAY_VEHICLES";
    private final VehicleBookingService vehicleBookingService;

    public DisplayVehicleCommandExecutor(VehicleBookingService vehicleBookingService) {
        this.vehicleBookingService = vehicleBookingService;
    }

    @Override
    public boolean isValid(Command command) {
        return DISPLAY_VEHICLES_COMMAND_NAME.equals(command.getCommandName()) &&
                command.getParams().size() == 3;
    }

    @Override
    public String execute(Command command) {
        List<String> params = command.getParams();
        String branchId = params.get(0);
        String startTime = params.get(1);
        String endTime = params.get(2);

        return vehicleBookingService.displayVehicleAtBranchForSlot(branchId, startTime, endTime);
    }
}
