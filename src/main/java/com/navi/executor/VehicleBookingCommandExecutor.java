package com.navi.executor;

import com.navi.enums.VehicleType;
import com.navi.model.Command;
import com.navi.service.VehicleBookingService;

import java.util.List;

public class VehicleBookingCommandExecutor extends CommandExecutor {

    public final static String BOOK_COMMAND_NAME = "BOOK";
    private VehicleBookingService vehicleBookingService;

    public VehicleBookingCommandExecutor(VehicleBookingService vehicleBookingService) {
        this.vehicleBookingService = vehicleBookingService;
    }

    @Override
    public boolean isValid(Command command) {
        return BOOK_COMMAND_NAME.equals(command.getCommandName()) &&
                command.getParams().size() == 4;
    }

    @Override
    public String execute(Command command) {
        List<String> params = command.getParams();
        String branchId = params.get(0);
        VehicleType vehicleType = VehicleType.fromString(params.get(1));
        String startTime = params.get(2);
        String endTime = params.get(3);

        return String.valueOf(vehicleBookingService.bookVehicle(branchId, vehicleType, startTime, endTime));
    }
}
