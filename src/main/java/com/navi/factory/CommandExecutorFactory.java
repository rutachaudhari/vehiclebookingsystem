package com.navi.factory;

import com.navi.exception.InvalidCommandException;
import com.navi.executor.*;
import com.navi.model.Command;
import com.navi.service.BranchManagementService;
import com.navi.service.VehicleBookingService;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CommandExecutorFactory {

    private Map<String, CommandExecutor> commandExecutorMap;

    public CommandExecutorFactory(BranchManagementService branchManagementService, VehicleBookingService vehicleBookingService) {
        this.commandExecutorMap = new HashMap<>();
        commandExecutorMap.put(AddBranchCommandExecutor.ADD_BRANCH_COMMAND_NAME, new AddBranchCommandExecutor(branchManagementService));
        commandExecutorMap.put(AddVehicleCommandExecutor.ADD_VEHICLE_COMMAND_NAME, new AddVehicleCommandExecutor(branchManagementService));
        commandExecutorMap.put(VehicleBookingCommandExecutor.BOOK_COMMAND_NAME, new VehicleBookingCommandExecutor(vehicleBookingService));
        commandExecutorMap.put(DisplayVehicleCommandExecutor.DISPLAY_VEHICLES_COMMAND_NAME, new DisplayVehicleCommandExecutor(vehicleBookingService));
    }

    /**
     * given a command returns respective command executor for executing the command
     * @param command
     * @return
     * @throws InvalidCommandException
     */
    public CommandExecutor getCommandExecutor(Command command) throws InvalidCommandException {
        CommandExecutor commandExecutor = commandExecutorMap.get(command.getCommandName());
        if (Objects.isNull(commandExecutor)) {
            throw new InvalidCommandException("No command executor found for " +command.getCommandName());
        }
        return commandExecutor;
    }


}
