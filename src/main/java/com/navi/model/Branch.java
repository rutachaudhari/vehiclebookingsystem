package com.navi.model;

import com.navi.enums.VehicleType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Branch {

    private String branchId;
    private Map<VehicleType, List<Vehicle>> availableVehicles;

    public Branch(String branchId, List<VehicleType> vehicleTypes) {
        this.branchId = branchId;
        Map<VehicleType, List<Vehicle>> availableVehicles = new HashMap<>();
        for (VehicleType vehicleType : vehicleTypes) {
            availableVehicles.put(vehicleType, new ArrayList<>());
        }
        this.availableVehicles = availableVehicles;
    }

    public String getBranchId() {
        return branchId;
    }

    public Map<VehicleType, List<Vehicle>> getAvailableVehicles() {
        return availableVehicles;
    }
}
