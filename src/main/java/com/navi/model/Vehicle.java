package com.navi.model;

import com.navi.enums.VehicleType;

import java.math.BigDecimal;

public class Vehicle {

    private String vehicleId;
    private VehicleType vehicleType;
    private BigDecimal price;

    public Vehicle(String vehicleId, VehicleType vehicleType, BigDecimal price) {
        this.vehicleId = vehicleId;
        this.vehicleType = vehicleType;
        this.price = price;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
