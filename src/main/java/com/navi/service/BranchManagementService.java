package com.navi.service;

import com.navi.enums.VehicleType;
import com.navi.model.Branch;
import com.navi.model.Vehicle;

import java.math.BigDecimal;
import java.util.*;

public class BranchManagementService {

    private Map<String, Branch> branchIdBranchMap;

    public BranchManagementService() {
        this.branchIdBranchMap = new HashMap<>();
    }

    /**
     * given branch id and vehicle types, method creates new branch
     * @param branchId
     * @param vehicleTypes
     * @return
     */
    public boolean addBranch(String branchId, List<VehicleType> vehicleTypes) {
        if(branchIdBranchMap.containsKey(branchId)){
            return false;
        }
        Branch branch = new Branch(branchId, vehicleTypes);
        branchIdBranchMap.put(branchId, branch);
        return true;
    }

    /**
     * given branch id, returns branch
     * @param branchId
     * @return
     */
    public Branch getBranchById(String branchId) {
        return branchIdBranchMap.get(branchId);
    }

    /**
     * given branch id, and vehicle details, method creates new vehicle and adds it to the branch
     * @param branchId
     * @param vehicleType
     * @param vehicleId
     * @param price
     * @return
     */
    public boolean addVehicleToBranch(String branchId, VehicleType vehicleType, String vehicleId, BigDecimal price) {
        if(!isVehicleSupportedByBranch(branchId, vehicleType)) {
            return false;
        }
        Branch branch = getBranchById(branchId);
        Map<VehicleType, List<Vehicle>> availableVehicleMap = branch.getAvailableVehicles();
        Vehicle newVehicle = new Vehicle(vehicleId, vehicleType, price);
        List<Vehicle> availableVehicles = availableVehicleMap.get(vehicleType);


        if(availableVehicles.stream().anyMatch(v -> v.getVehicleId().equals(vehicleId))) {
            //vehicle id already exists at branch
            return false;
        }
        availableVehicles.add(newVehicle);
        availableVehicleMap.put(vehicleType, availableVehicles);
        return true;
    }

    /**
     * checks if a given vehicle type is supported by branch
     * @param branchId
     * @param vehicleType
     * @return
     */
    public boolean isVehicleSupportedByBranch(String branchId, VehicleType vehicleType) {
        if(Objects.isNull(vehicleType)) return false;
        Branch branch = getBranchById(branchId);
        if(Objects.isNull(branch)){
            return false;
        }
        return branch.getAvailableVehicles().containsKey(vehicleType);
    }
}
