package com.navi.service;

import com.navi.exception.InvalidCommandException;
import com.navi.exception.NoSuchFileException;
import com.navi.executor.CommandExecutor;
import com.navi.factory.CommandExecutorFactory;
import com.navi.model.Command;

import java.io.*;


public class FileReaderService {

    private String fileName;
    private CommandExecutorFactory commandExecutorFactory;

    public FileReaderService(String fileName, CommandExecutorFactory commandExecutorFactory) {
        this.fileName = fileName;
        this.commandExecutorFactory = commandExecutorFactory;
    }

    /**
     * method reads commands from the file and executes them sequentially
     * @throws NoSuchFileException
     * @throws IOException
     * @throws InvalidCommandException
     */
    public void process() throws NoSuchFileException, IOException, InvalidCommandException {
        File file = new File(fileName);
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            throw new NoSuchFileException("filename: " + fileName + " not found!" + " error-message: " + e.getMessage());
        }

        String input = reader.readLine();
        while (input != null) {
            Command command = new Command(input);
            processCommand(command);
            input = reader.readLine();
        }
    }

    protected void processCommand(final Command command) throws InvalidCommandException {
        CommandExecutor commandExecutor = commandExecutorFactory.getCommandExecutor(command);
        if (commandExecutor.isValid(command)) {
            String output = commandExecutor.execute(command);
            System.out.println(output);
        } else {
            throw new InvalidCommandException("Command" +command + "is not valid!");
        }
    }
}
