package com.navi.service;

import com.navi.enums.VehicleType;
import com.navi.model.Branch;
import com.navi.model.Vehicle;
import com.navi.strategy.DefaultPricingStrategy;
import com.navi.strategy.DefaultVehicleSelectionStrategy;
import com.navi.strategy.PricingStrategy;
import com.navi.strategy.VehicleSelectionStrategy;

import java.math.BigDecimal;
import java.util.*;

public class VehicleBookingService {
    //B1_V1 -> [0-23]
    private Map<String, List<Integer>> vehicleBooking;
    private BranchManagementService branchManagementService;
    private VehicleSelectionStrategy vehicleSelectionStrategy;
    private PricingStrategy pricingStrategy;

    public VehicleBookingService(BranchManagementService branchManagementService) {
        this.vehicleBooking = new HashMap<>();
        this.branchManagementService = branchManagementService;
        this.vehicleSelectionStrategy = new DefaultVehicleSelectionStrategy();
        this.pricingStrategy = new DefaultPricingStrategy();
    }

    /**
     * given branch id ,vehicle and slot details. Method books vehicle and returns a booking price
     * @param branchId
     * @param vehicleType
     * @param startTime
     * @param endTime
     * @return
     */
    public BigDecimal bookVehicle(String branchId, VehicleType vehicleType, String startTime, String endTime) {
        if(!isSlotValid(startTime, endTime)) {
            return BigDecimal.valueOf(-1);
        }
        if (!branchManagementService.isVehicleSupportedByBranch(branchId, vehicleType)) {
            return BigDecimal.valueOf(-1);
        }
        Branch branch = branchManagementService.getBranchById(branchId);
        Map<VehicleType, List<Vehicle>> availableVehicleMap = branch.getAvailableVehicles();
        List<Vehicle> vehiclesForVehicleType = availableVehicleMap.get(vehicleType);

        if(Objects.isNull(vehiclesForVehicleType) || vehiclesForVehicleType.isEmpty()){
            return BigDecimal.valueOf(-1);
        }
        List<Vehicle> vehiclesAvailableInSlot = new ArrayList<>();

        for (Vehicle vehicle : vehiclesForVehicleType) {
            if (isVehicleAvailableInSlot(branchId, vehicle, startTime, endTime)) {
                vehiclesAvailableInSlot.add(vehicle);
            }
        }

        Optional<Vehicle> vehicle = vehicleSelectionStrategy.getPreferredVehicle(vehiclesAvailableInSlot);
        if(!vehicle.isPresent()) {
           return BigDecimal.valueOf(-1);
        }
        updateVehicleAvailabilityForSlot(branchId, vehicle.get(), startTime, endTime);

        BigDecimal price =  pricingStrategy.calculatePrice(vehicle.get());
        BigDecimal startTimeHours = BigDecimal.valueOf(Integer.parseInt(startTime));
        BigDecimal endTimeHours = BigDecimal.valueOf(Integer.parseInt(endTime));

        return price.multiply(endTimeHours.subtract(startTimeHours));

    }

    /**
     * method displays all the available vehicle ids in the given branch for a slot
     * @param branchId
     * @param startTime
     * @param endTime
     * @return
     */
    public String displayVehicleAtBranchForSlot(String branchId, String startTime, String endTime) {
        if(!isSlotValid(startTime, endTime)) {
            return "-1";
        }
        Branch branch = branchManagementService.getBranchById(branchId);
        Map<VehicleType, List<Vehicle>> availableVehiclesMap = branch.getAvailableVehicles();
        List<Vehicle> vehicleList = new ArrayList<>();
        for (List<Vehicle> vehicles : availableVehiclesMap.values()) {
            vehicleList.addAll(vehicles);
        }

        List<String> availableVehicleIdList = new ArrayList<>();
        for(Vehicle vehicle: vehicleList) {
            if(isVehicleAvailableInSlot(branchId, vehicle, startTime, endTime)) {
                availableVehicleIdList.add(vehicle.getVehicleId());
            }
        }
        String availableVehicleIds = String.join(", " ,  availableVehicleIdList);
        return availableVehicleIds;
    }

    private boolean isSlotValid(String startTime, String endTime) {
        int st = Integer.parseInt(startTime);
        int et = Integer.parseInt(endTime);
        return st >= 0 && et <= 23 && et >= st;
    }

    private void updateVehicleAvailabilityForSlot(String branchId, Vehicle vehicle, String startTime, String endTime) {
        String mapKey = getMapKey(branchId, vehicle.getVehicleId());
        List<Integer> slots = vehicleBooking.get(mapKey);
        if(Objects.isNull(slots)) {
            slots = new ArrayList<Integer>(Collections.nCopies(23, 0));
        }
        for (int i = Integer.parseInt(startTime); i < Integer.parseInt(endTime); i++) {
            slots.set(i, 1);
        }
        vehicleBooking.put(mapKey, slots);
    }

    private boolean isVehicleAvailableInSlot(String branchId, Vehicle vehicle, String startTime, String endTime) {
        String mapKey = getMapKey(branchId, vehicle.getVehicleId());
        List<Integer> slots = vehicleBooking.get(mapKey);
        if (!Objects.isNull(slots)) {
            for(int i = Integer.parseInt(startTime); i < Integer.parseInt(endTime) - 1; i++) {
                if(slots.get(i) == 1) {
                    return false;
                }
            }
        }
        return true;
    }

    private String getMapKey(String branchId, String vehicleId) {
        return String.format("%s_%s", branchId, vehicleId);
    }
}
