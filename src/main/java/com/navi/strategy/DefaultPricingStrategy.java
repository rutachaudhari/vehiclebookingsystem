package com.navi.strategy;

import com.navi.model.Vehicle;

import java.math.BigDecimal;

/**
 * calculates price for given vehicle based on actual vehicle price
 */
public class DefaultPricingStrategy implements PricingStrategy {

    @Override
    public BigDecimal calculatePrice(Vehicle vehicle) {
        return vehicle.getPrice();
    }
}
