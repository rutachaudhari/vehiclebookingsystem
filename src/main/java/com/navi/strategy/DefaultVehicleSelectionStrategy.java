package com.navi.strategy;

import com.navi.model.Vehicle;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * returns vehicle having minimum price
 */
public class DefaultVehicleSelectionStrategy implements VehicleSelectionStrategy {
    private PricingStrategy pricingStrategy;

    public DefaultVehicleSelectionStrategy() {
        this.pricingStrategy = new DefaultPricingStrategy();
    }

    @Override
    public Optional<Vehicle> getPreferredVehicle(List<Vehicle> vehicles) {
        if(Objects.isNull(vehicles)) {
            return Optional.empty();
        }
        Optional<Vehicle> minPriceVehicle = vehicles.stream().min(Comparator.comparing(v -> pricingStrategy.calculatePrice(v)));
        return minPriceVehicle;
    }
}
