package com.navi.strategy;

import com.navi.model.Vehicle;

import java.math.BigDecimal;
import java.util.List;

public interface PricingStrategy {

    /**
     * method calculates the price for a vehicle based on pricing strategy
     * @param vehicle
     * @return
     */
    BigDecimal calculatePrice(Vehicle vehicle);


}
