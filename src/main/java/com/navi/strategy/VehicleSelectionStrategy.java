package com.navi.strategy;

import com.navi.model.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleSelectionStrategy {

    /**
     * method returns a preferred vehicle based on given vehicle list
     * @param vehicle
     * @return
     */
    Optional<Vehicle> getPreferredVehicle(List<Vehicle> vehicle);
}
