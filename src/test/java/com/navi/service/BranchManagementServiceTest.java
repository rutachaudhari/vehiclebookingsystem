package com.navi.service;

import com.navi.enums.VehicleType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BranchManagementServiceTest {

    private final BranchManagementService branchManagementService;

    BranchManagementServiceTest() {
        this.branchManagementService = new BranchManagementService();
    }

    @Test
    void addNewBranch() {
        List<VehicleType> vehicleTypeList = new ArrayList<>();
        vehicleTypeList.add(VehicleType.BIKE);
        vehicleTypeList.add(VehicleType.VAN);

        assertTrue(branchManagementService.addBranch("B1", vehicleTypeList));
    }

    @Test
    void addBranchWhenBranchIdAlreadyExist() {
        List<VehicleType> vehicleTypeList = new ArrayList<>();
        vehicleTypeList.add(VehicleType.BIKE);
        vehicleTypeList.add(VehicleType.VAN);

        branchManagementService.addBranch("B1", vehicleTypeList);
        assertFalse(branchManagementService.addBranch("B1", vehicleTypeList));
    }

    @Test
    void getBranchById() {
        assertNull(branchManagementService.getBranchById("B1"));
    }

    @Test
    void addVehicleToBranchWhenVehicleNotSupported() {
        assertFalse(branchManagementService.addVehicleToBranch("B1", VehicleType.BIKE, "V1", BigDecimal.TEN));
    }

    @Test
    void addVehicleToBranchWhenVehicleIdExistAtBranch() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.BIKE));
        branchManagementService.addVehicleToBranch("B1", VehicleType.BIKE, "V1", BigDecimal.TEN);
        assertFalse(branchManagementService.addVehicleToBranch("B1", VehicleType.BIKE, "V1", BigDecimal.TEN));
    }

    @Test
    void addVehicleToBranch() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.BIKE));
        assertTrue(branchManagementService.addVehicleToBranch("B1", VehicleType.BIKE, "V1", BigDecimal.TEN));
    }

    @Test
    void isVehicleSupportedByBranchWhenVehicleTypeNull() {
        assertFalse(branchManagementService.isVehicleSupportedByBranch("B1", null));
    }

    @Test
    void isVehicleSupportedByBranchWhenBranchDoesNotExist() {
        assertFalse(branchManagementService.isVehicleSupportedByBranch("B1", VehicleType.BIKE));
    }

    @Test
    void isVehicleSupportedByBranchWhenBranchDoesHaveVehicleType() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));
        assertFalse(branchManagementService.isVehicleSupportedByBranch("B1", VehicleType.BIKE));
    }

    @Test
    void isVehicleSupportedByBranch() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.BIKE));
        assertTrue(branchManagementService.isVehicleSupportedByBranch("B1", VehicleType.BIKE));
    }
}