package com.navi.service;

import com.navi.enums.VehicleType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VehicleBookingServiceTest {

    private final VehicleBookingService vehicleBookingService;
    private final BranchManagementService branchManagementService;

    VehicleBookingServiceTest() {
        this.branchManagementService = new BranchManagementService();
        this.vehicleBookingService = new VehicleBookingService(branchManagementService);
    }

    @Test
    void bookVehicleWhenSlotOutOfRange() {
        assertEquals(BigDecimal.valueOf(-1), vehicleBookingService.bookVehicle("B1", VehicleType.BIKE, "-1", "4"));
    }

    @Test
    void bookVehicleWhenVehicleTypeUnavailable() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));

        assertEquals(BigDecimal.valueOf(-1), vehicleBookingService.bookVehicle("B1", VehicleType.VAN, "1", "3"));
    }

    @Test
    void bookVehicleWhenVehicleTypeAvailable() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V1", BigDecimal.TEN);

        assertEquals(BigDecimal.valueOf(20), vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3"));
    }

    @Test
    void bookVehicleWhenSlotAvailable() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V1", BigDecimal.TEN);

        assertEquals(BigDecimal.valueOf(20), vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3"));
    }

    @Test
    void bookVehicleWhenSlotUnavailable() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V1", BigDecimal.TEN);

        vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3");
        assertEquals(BigDecimal.valueOf(-1), vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3"));
    }

    @Test
    void bookVehicleWhenMultipleVehiclesAvailableForSameSlot() {
        branchManagementService.addBranch("B1", Collections.singletonList(VehicleType.CAR));
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V1", BigDecimal.TEN);
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V2", BigDecimal.ONE);

        assertEquals(BigDecimal.valueOf(2), vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3"));
    }

    @Test
    void displayVehicleAtBranchForSlot() {
        branchManagementService.addBranch("B1", Arrays.asList(VehicleType.CAR, VehicleType.BIKE));
        branchManagementService.addVehicleToBranch("B1", VehicleType.CAR, "V1", BigDecimal.TEN);
        branchManagementService.addVehicleToBranch("B1", VehicleType.BIKE, "V2", BigDecimal.ONE);
        vehicleBookingService.bookVehicle("B1", VehicleType.CAR, "1", "3");

        assertEquals("V2", vehicleBookingService.displayVehicleAtBranchForSlot("B1", "1", "3"));
    }
}