package com.navi.strategy;

import com.navi.enums.VehicleType;
import com.navi.model.Vehicle;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultPricingStrategyTest {

    private final DefaultPricingStrategy defaultPricingStrategy;

    DefaultPricingStrategyTest() {
        this.defaultPricingStrategy = new DefaultPricingStrategy();
    }

    @Test
    void calculatePrice() {
        Vehicle vehicle = new Vehicle("V1", VehicleType.BIKE, BigDecimal.TEN);

        assertEquals(BigDecimal.TEN, defaultPricingStrategy.calculatePrice(vehicle));
    }
}