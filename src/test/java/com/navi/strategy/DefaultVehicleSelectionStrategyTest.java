package com.navi.strategy;

import com.navi.enums.VehicleType;
import com.navi.model.Vehicle;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultVehicleSelectionStrategyTest {

    private final DefaultVehicleSelectionStrategy defaultVehicleSelectionStrategy;

    DefaultVehicleSelectionStrategyTest() {
        this.defaultVehicleSelectionStrategy = new DefaultVehicleSelectionStrategy();
    }


    @Test
    void getPreferredVehicle() {
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle_V1 = new Vehicle("V1", VehicleType.BIKE, BigDecimal.TEN);
        Vehicle vehicle_V2 = new Vehicle("V2", VehicleType.CAR, BigDecimal.ONE);
        vehicles.add(vehicle_V1);
        vehicles.add(vehicle_V2);

        Optional<Vehicle> vehicleOptional = defaultVehicleSelectionStrategy.getPreferredVehicle(vehicles);
        Vehicle vehicle = vehicleOptional.get();

        assertEquals("V2", vehicle.getVehicleId());
    }
}